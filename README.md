# Kaniko sample for GitLab CI on AWS Fargate

Sample repository demonstrating how to set up GitLab CI to __build Docker
images with Kaniko__, using the [AWS Fargate Custom Executor driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate)
for [GitLab Runner](https://docs.gitlab.com/runner).

The image includes demo-purpose-only dependencies.

**WORK IN PROGRESS!** This solution is not fully operational yet (please visit Fargate driver
[MR!34](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate/-/merge_requests/34)
for further details).

## Known issues

1. Installing `openssh-server` in the resulting image breaks the build. Is this
   related to any Kaniko issue?
